# Combinatorics

* [Source](http://github.com/postmodern/combinatorics)
* [Issues](http://github.com/postmodern/combinatorics/issues)
* [Documentation](http://rubydoc.info/gems/combinatorics)
* [Email](mailto:postmodern.mod3 at gmail.com)

## Description

A collection of modules and methods for performing
[Combinatoric](http://en.wikipedia.org/wiki/Combinatoric) calculations.
Methods are defined to compute power sets, cartesian products, permutations, 
combinations, and derangements. Note: this includes k-combinations and 
k-permutations, i.e. only generating resultant sets of a given size. Each
set theory operation method supports block arguments to allow continuous code
execution during the combinatoric set generation process. Each combinatoric 
operation implementation also supports a `cardinality` or `len` method which 
determines the total number of subsets that will be created beforehand (to aid
in the avoidance of starting an operation which will never complete due to 
exponential computational complexity.)

## Features

* Adds Haskell/Python style list comprehensions via {Array#comprehension}.
* Provides reusable Combinatorics Mixins:
  * {Combinatorics::CartesianProduct}
  * {Combinatorics::Choose}
  * {Combinatorics::Derange}
  * {Combinatorics::Permute}
  * {Combinatorics::PowerSet}
* Adds Combinatorics conveniance methods to {Array} and {Set}:
  * `cartprod`
  * `choose`
  * `derange`
  * `permute`
  * `powerset`
* Adds {Range#&}, {Range#upto} and {Range#downto}.

## Examples

Power-set of an {Array}:

    require 'combinatorics/power_set'

    [1,2,3,4].powerset
    # => [[],
          [4],
          [3],
          [3, 4],
          [2],
          [2, 4],
          [2, 3],
          [2, 3, 4],
          [1],
          [1, 4],
          [1, 3],
          [1, 3, 4],
          [1, 2],
          [1, 2, 4],
          [1, 2, 3],
          [1, 2, 3, 4]]

Power-set of a {Set}:

    Set['ab', 'cd', 'ef'].powerset
    # => [#<Set: {}>,
          #<Set: {"ef"}>,
          #<Set: {"cd"}>,
          #<Set: {"cd", "ef"}>,
          #<Set: {"ab"}>,
          #<Set: {"ab", "ef"}>,
          #<Set: {"ab", "cd"}>,
          #<Set: {"ab", "cd", "ef"}>]

Find the intersecting sub-range between two ranges:

    (1..50) & (20..100)
    # => (20..50)

Enumerate over every sub-range between two ranges:

    (1..5).upto(2..10).to_a
    # => [1..5, 1..6, 1..7, 1..8, 1..9, 1..10,
          2..5, 2..6, 2..7, 2..8, 2..9, 2..10]

List comprehensions:

    require 'combinatorics/list_comprehension'

    [(0..10).step(2),('a'..'c')].comprehension.to_a
    # => [[0, "a"],
          [0, "b"],
          [0, "c"],
          [2, "a"],
          [2, "b"],
          [2, "c"],
          [4, "a"],
          [4, "b"],
          [4, "c"],
          [6, "a"],
          [6, "b"],
          [6, "c"],
          [8, "a"],
          [8, "b"],
          [8, "c"],
          [10, "a"],
          [10, "b"],
          [10, "c"]]

Cartesian products:

require 'combinatorics/cartesian_product'

['a', 'b', 'c'].cartprod([0, 1, 2]).to_a
# => [["a", 0], 
      ["b", 0], 
      ["c", 0], 
      ["a", 1], 
      ["b", 1], 
      ["c", 1], 
      ["a", 2], 
      ["b", 2], 
      ["c", 2]]

k-combinations:

require 'combinatorics/choose'

('a'..'f').to_a.choose(2).to_a
# => [["a", "b"], 
      ["a", "c"], 
      ["a", "d"], 
      ["a", "e"], 
      ["a", "f"], 
      ["b", "c"], 
      ["b", "d"], 
      ["b", "e"], 
      ["b", "f"], 
      ["c", "d"], 
      ["c", "e"], 
      ["c", "f"], 
      ["d", "e"], 
      ["d", "f"], 
      ["e", "f"]]

Derangements:

require 'combinatorics/derange'

[:_, :q, :z, :x].derange.to_a
# => [[:q, :_, :x, :z], 
      [:q, :z, :x, :_], 
      [:q, :x, :_, :z], 
      [:z, :_, :x, :q], 
      [:z, :x, :_, :q], 
      [:z, :x, :q, :_], 
      [:x, :_, :q, :z], 
      [:x, :z, :_, :q], 
      [:x, :z, :q, :_]]

Permutation cardinality:

require 'combinatorics/permutation'

include Combinatorics::Permute

Combinatorics::Permute::cardinality(128)
# => 8256


## Requirements

  * [ruby](http://www.ruby-lang.org/) >= 1.8.7

## Install

      $ sudo gem install combinatorics

## Copyright

Copyright (c) 2010 Hal Brodigan

See {file:LICENSE.txt} for license information.
